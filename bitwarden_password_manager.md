# What is BitWarden?

    Bitwarden is a free/freemium open-source password management service that stores sensitive information such as website credentials in an encrypted vault.

### Here is what the web interface of BitWarden looks like

![image.png](./img/bitwarden_interface.png)


# How to create an account for BitWarden

Go to the BitWarden website: [here](https://vault.bitwarden.com/#/register?layout=default)

Go through these steps:

![image.png](./img/create_bitw.png)



1. Signup using an email address ([I suggest getting a free ProtonMail email account](https://account.proton.me/signup?plan=free&billing=12&minimumCycle=12&currency=undefined&language=en))
2. Enter whatever you want for your name (so long as we can verify you are a community member it's fine)
3. Create a main password for your BitWarden account ([please go here for a good password generator](https://bitwarden.com/password-generator/))
4. Re-type that same password
5. Create a password hint if you feel like you need it
6. Check this box to agree to the account terms
7. Create account




## "I have an account-- now what?"

### Now that you have an account it's up to you! Here are some things to do:

- Contact IT/Arbiters by emailing it_support[at]foodforest[dot]ngo to ask for access to BFF login infos you need for your BFF work
- Start transferring your logins to BitWarden - [Follow this guide by BitWarden on how to save logins to your password vault](https://bitwarden.com/help/managing-items/)

## How BFF uses BitWarden

There is a shared vault for BFF logins owned by the Board through a joint BitWarden account and myself, D, arbiter and Board member (the free version of BitWarden allows for only two owners of a shared vault).

There are also shared vaults created for certain committees. If you would like to get a shared vault for your committee please reach out to IT Arbiters by emailing it_support[at]foodforest[dot]ngo as well as your committee leadership.
